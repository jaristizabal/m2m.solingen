package com.n6consulting.m2m.request;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;

@SpringBootApplication
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
// BEGIN ADD with-action-links
  @Bean
  public ResourceProcessor<Resource<Request>> requestProcessor() {

    return new ResourceProcessor<Resource<Request>>() {

      @Override
      public Resource<Request> process(Resource<Request> resource) {
        RequestController.addActionLinks(resource);
        return resource;
      }
    };
  }
// END ADD with-action-links
}

