package com.n6consulting.m2m.request;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource
@Repository
public interface RequestRepository extends PagingAndSortingRepository<Request, Long> {
  // BEGIN ADD find-by-status
  @RestResource(path = "status")
  Iterable<Request> findByStatus(@Param("q") Request.Status status);
  // END ADD find-by-status
}