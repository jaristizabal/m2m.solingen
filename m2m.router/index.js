const express = require('express')
const proxy = require('express-http-proxy')
const url = require('url')

var app = express()

app.use('/modlog', proxy(process.env.MODLOG_DNS_NAME))
app.use('/*', proxy(process.env.LOBSTERS_DNS_NAME, {
    forwardPath: req => url.parse(req.baseUrl).path,
    preserveHostHdr: true
}))

app.listen(1972, () => console.log('Listening on port 1972'))
