# Monolith-to-Microservices Welcome

This directory is where you will do all your work for this class. In the beginning, it contains only a little bit of code and infrastructure scripting. By the end of the week, it will be home to multiple services and their deployment configurations.

## Getting Set up

There are two main requirements for everything to work.

First, you must have Docker installed and working. Visit [Docker.com](https://docker.com) to download the Community Edition. It is called "Docker CE". You do not need Docker Enterprise for this class.

Second, you must have the AWS command line tools installed. See [AWS Command Line Interface](https://aws.amazon.com/cli/) for instructions to download and set up these tools.


## Folders

`lobsters` contains the code and configuration for our monolith. This is a Ruby on Rails application.

`infrastructure` contains configuration scripts and tools we will use to run a local network of services and provision a real production environment on AWS.

## About "Monorepos"

Because we are putting all our services in a single version control repository, this is what's known as a "monorepo."

There is plenty of controversy about whether a monorepo is a good idea or a bad one.

For the purposes of this class we use a monorepo just to make it easier to teach. Whether you want to use a monorepo for your own system is your choice.