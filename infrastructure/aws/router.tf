data "template_file" "router-run-sh" {
  template = "${file("${path.module}/templates/router-run-sh.tpl")}"
  vars {
    modlog_dns_name = "www.google.com"
    lobsters_dns_name = "${aws_lb.lb-lobsters.dns_name}"
  }
}
