#cloud-config
write_files:
  - encoding: b64
    path: /home/ubuntu/app/run.sh
    content: "${run_script}"
    permissions: '0755'

package_upgrade: true

packages:
  - awscli
  - nodejs

runcmd:
  - mkdir -p /home/ubuntu/app
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/${router_service_tarball}" /home/ubuntu/app/
  - cd /home/ubuntu/app && tar xvzf ${router_service_tarball}
  - cd /home/ubuntu/app && ./run.sh
